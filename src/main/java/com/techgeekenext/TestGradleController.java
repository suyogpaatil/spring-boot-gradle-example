package com.techgeekenext;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestGradleController {

    @GetMapping(value = "/welcome")
    public String hello() {
        return "TechGeekNext - Welcome to Spring boot Gradle Demo new auto build..testing release. Latest updated";
    }

    @GetMapping(value = "/welcome-back")
    public String welcomeBack() {
        return "Welcome back to spring gradle demo project";
    }
    
    @GetMapping(value = "/greet-morning")
    public String greetMorning() {
        return "Good morning team!!";
    }
}
